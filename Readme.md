# python_docker_api
---
### Use python to control the docker server to automatically ...

* Create container with specific variable.      
* Return the desire URL instead of localhost.   
* Delete container by ID.   


## Install
```
sudo apt install python3
sudo apt install python3-pip
pip3 install docker
```

## Usage

* Need sudo auth to use docker sdk
```
sudo python3
```

## Ref
> https://docs.docker.com/develop/sdk/examples/

> https://docker-py.readthedocs.io/en/stable/

