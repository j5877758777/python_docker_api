from requests import get
import docker
import time

ip = get('https://api.ipify.org').text # Get current host ip
client = docker.from_env()


def run_jupyter():
    '''
    Return jupyter server url
    '''
    container = client.containers.run('jupyter/datascience-notebook:latest', detach=True, ports={'8888/tcp':8888})
    
    # Wait for jupyter server finish initialize log
    while True:
        log_list = container.logs().decode("utf-8").split('\n')
        if len(log_list) >=6 :
            break
        time.sleep(0.1) # sleep for 0.1s
    
    url = log_list[6]
    start_pos = url.find('http')
    url = url[start_pos:]

    # Change (127.0.0.1 ..) to Host IP
    start_pos = url.find('(')
    end_pos = url.find(')')
    url = url[0:start_pos] + ip + url[end_pos+1:]
    url = url.replace(' ', '')
    return url

def run(image_name):
    container = client.containers.run(image_name, detach=True, ports={'8888/tcp':8888})
    return container.id

def kill(container_id):
    container = client.containers.get(container_id)
    try:
        container.kill()
    except Exception as e:
        print("{}: {}".format(e.__class__.__name__, e))
    return "success"

def containers_list():
    return client.containers.list()

def images_list():
    return client.images.list()

